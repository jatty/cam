import logging
import sys
from logging import debug, info, warn, warning, error


def setuplogger(level=logging.INFO):
    logging.basicConfig(
        stream=sys.stdout, style='{', format='{asctime}: {levelname:>7s} {threadName:24s} {module:>9s}:{lineno:>4d} {funcName:22s} {message}', level=level)
