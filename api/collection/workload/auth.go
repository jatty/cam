package workload

import (
	"fmt"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

// AuthFromKubeConfig returns the kube clientset for the given kube config
func AuthFromKubeConfig(kubeConfig []byte) (clientset *kubernetes.Clientset, err error) {
	clientset = &kubernetes.Clientset{}

	// get kube config
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeConfig)
	if err != nil {
		return clientset, fmt.Errorf("could not get kubernetes config: %w", err)
	}

	// create the clientset
	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		return clientset, fmt.Errorf("could not create client: %w", err)
	}

	return
}
