package common_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/proto"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/testutil"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/testutil/testproto"
)

const MockEvidenceID = "00000000-0000-0000-0000-000000000000"

func Test_PersistEvidence(t *testing.T) {
	var (
		err error
		e2  common.Evidence
	)

	storage := testutil.NewInMemoryStorage(t)

	e := common.Evidence{
		Id:    MockEvidenceID,
		Name:  "my evidence",
		Value: testproto.ToValue(t, struct{ Test string }{Test: "test"}),
	}

	err = storage.Save(&e)
	assert.NoError(t, err)

	err = storage.Get(&e2, "id = ?", MockEvidenceID)
	assert.NoError(t, err)

	assert.True(t, proto.Equal(&e, &e2))
}
